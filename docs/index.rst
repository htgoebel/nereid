:orphan:

Welcome to Nereid |version|
===========================


Welcome to Nereid's documentation.  This documentation is divided into
different parts.  We recommend that you get started with
:ref:`installation` and then head over to :ref:`quickstart`.
If you'd rather dive into the internals of Nereid, check out
the :ref:`api` documentation.
 
The main dependencies of Nereid are `Flask`_ and `Tryton`_. In addition,
Nereid uses `Babel`_ for internationalisation, `Speak Later`_ for lazy
strings and `WTForms`_ for form building.

The template engine used is `Jinja2`_ and `Flask`_ is built over `Werkzeug`_ 
a WSGI toolkit.These libraries are not documented here.  If you want to dive 
into their documentation, check out the following links:

-   `Flask Documentation <https://flask.palletsprojects.com/>`_
-   `Tryton Documentation <https://docs.tryton.org/>`_
-   `Babel Documentation <https://babel.pocoo.org/>`_
-   `Speaklater Documentation <https://pypi.python.org/pypi/speaklater>`_
-   `WTForms Documentation <https://wtforms.readthedocs.io/>`_
-   `Jinja2 Documentation <https://jinja.palletsprojects.com/>`_

If this documentation reminds you of `Flask`_ documentation, it is not a
coincidence, it has been designed so to ease up the learning curve for
anyone who knows both `Flask`_ and `Tryton`_.

.. _Flask: https://flask.palletsprojects.com/
.. _Tryton: https://www.tryton.org
.. _WTForms: https://wtforms.readthedocs.io
.. _Babel: https://babel.pocoo.org/
.. _Speak Later: https://pypi.python.org/pypi/speaklater
.. _ccy: http://pypi.python.org/pypi/ccy
.. _Jinja2: https://jinja.palletsprojects.com/
.. _Werkzeug: https://werkzeug.palletsprojects.com/

.. include:: contents.rst.inc
